import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TimeAgoComponent } from './time-ago.component';

@NgModule({
  declarations: [TimeAgoComponent],
  imports: [
    CommonModule
  ],
  exports: [TimeAgoComponent]
})
export class TimeAgoModule { }
