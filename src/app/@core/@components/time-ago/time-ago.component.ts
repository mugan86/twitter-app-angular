import { Component, OnInit, Input } from '@angular/core';
import { DataManageService } from '@core/services/data-manage.service';

@Component({
  selector: 'app-time-ago',
  templateUrl: './time-ago.component.html',
  styleUrls: ['./time-ago.component.css']
})
export class TimeAgoComponent implements OnInit {
  @Input() when: string = '2019-11-29T18:00:19.739+00:00';
  passTime: string;
  constructor(private dataManage: DataManageService) { }

  ngOnInit() {
    this.passTime = this.dataManage.getPassTimeFromNow(this.when);
  }

}
