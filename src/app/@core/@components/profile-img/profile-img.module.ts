import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileImgComponent } from './profile-img.component';

@NgModule({
  declarations: [ProfileImgComponent],
  imports: [
    CommonModule
  ],
  exports: [ProfileImgComponent]
})
export class ProfileImgModule { }
