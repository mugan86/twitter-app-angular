import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-profile-img',
  templateUrl: './profile-img.component.html',
  styleUrls: ['./profile-img.component.css']
})
export class ProfileImgComponent implements OnInit {
  @Input() size = 'medium';
  constructor() { }

  ngOnInit() {
  }

}
