import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AnimatedProgressBarComponent } from './animated-progress-bar.component';



@NgModule({
  declarations: [AnimatedProgressBarComponent],
  imports: [
    CommonModule
  ],
  exports: [AnimatedProgressBarComponent]
})
export class AnimatedProgressBarModule { }
