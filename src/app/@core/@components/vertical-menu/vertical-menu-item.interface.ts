export interface IVerticalMenuItem {
    icon: string;
    title: string;
    badge?: boolean;
    route: string;
}
