import { Component, OnInit, Input } from '@angular/core';
import { IVerticalMenuItem } from './vertical-menu-item.interface';

@Component({
  selector: 'app-vertical-menu',
  templateUrl: './vertical-menu.component.html',
  styleUrls: ['./vertical-menu.component.css']
})
export class VerticalMenuComponent implements OnInit {
  @Input() verticalMenuItems: Array<IVerticalMenuItem> = [];
  constructor() { }

  ngOnInit() {
  }
  
  logout() {
    console.log('logout');
  }

}
