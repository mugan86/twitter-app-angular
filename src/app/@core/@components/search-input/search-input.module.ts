import { SearchInputComponent } from './search-input.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [ SearchInputComponent],
  imports: [
    CommonModule
  ],
  exports: [ SearchInputComponent]
})
export class SearchInputModule { }
