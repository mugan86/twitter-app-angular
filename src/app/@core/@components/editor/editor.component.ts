import { EMOJISSETS } from './editor.constants';
import { EditorService } from './editor.service';
import { Component, Input, Output, EventEmitter } from '@angular/core';
@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.css']
})
export class EditorComponent {
  @Input() textHtml = '';
  @Input() limit = 480;
  @Output() onChangeEditor: EventEmitter<string> = new EventEmitter<string>();
  charactersLimitPass: boolean = false;
  showEmojiPicker = false;
  contentLength = 0;
  set = EMOJISSETS.TWITTER;
  constructor(private editorService: EditorService) {}

  toggleEmojiPicker() {
    this.showEmojiPicker = !this.showEmojiPicker;
  }
  addChar(insertChar: string) {
    return this.textHtml.substring(0, this.editorService.focusPosition) + insertChar + this.textHtml.substring(this.editorService.focusPosition);
  }
  addEmoji(event) {
    this.textHtml += event.emoji.native;
    // console.log(this.editorService.focusPosition, this.textHtml.length);
    this.onChange(null);
  }
  // https://codepen.io/mugan86/pen/zYxmzOM
  onChange($event) {
    this.contentLength = this.textHtml.length;
    this.checkLimit();
    this.onChangeEditor.emit(this.textHtml);
  }
  onFocus() {
    this.showEmojiPicker = false;
  }
  onBlur() {
    console.log('onblur')
  }

  checkLimit() {
    if (this.contentLength > this.limit) {
      this.charactersLimitPass = true;
    } else {
      this.charactersLimitPass = false;
    }
  }
}
