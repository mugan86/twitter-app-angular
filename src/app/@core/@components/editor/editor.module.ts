import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditorComponent } from './editor.component';
import { PickerModule } from '@ctrl/ngx-emoji-mart';
@NgModule({
  declarations: [EditorComponent],
  imports: [
    CommonModule,
    FormsModule,
    PickerModule
  ],
  exports: [EditorComponent]
})
export class EditorModule { }
