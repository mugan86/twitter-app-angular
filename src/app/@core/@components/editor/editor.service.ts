import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EditorService {
  focusPosition = 0;
  words = [];
  allWordsElement = [];
  private checkExistBeforeWordContent(stringWithHtml: string, position: number, newWordContent: string) {
    return stringWithHtml[position - 1] !== undefined && newWordContent !== '';
  }

  private addWordAndReset(words: Array<string>, newWord: string) {
    words.push(newWord);
    newWord = '';
    return {
      words,
      newWord
    };
  }

  getExtractTagsAndContent(stringWithHtml: string) {
    let newWord = '';
    this.words = [];
    let clean;
    this.allWordsElement = [];
    for (let i = 0; i < stringWithHtml.length; i++) {
      if (stringWithHtml[i] === '<') { // Empezando con un tag
      if (this.checkExistBeforeWordContent(stringWithHtml,
        i, newWord)) {
        // Si estamos en el primer tag pero la palabra
        // tiene contenido, antes de concatenar, guardar en el array de palabras y resetear y después
        this.allWordsElement.push(this.words.length);
        clean = this.addWordAndReset(this.words, newWord);
        newWord = clean.newWord;
        this.words = clean.words;
      }
      newWord += stringWithHtml[i];
    } else if (stringWithHtml[i] === '>') {
      // console.log('end');
      newWord += stringWithHtml[i];
      clean = this.addWordAndReset(this.words, newWord);
      newWord = clean.newWord;
      this.words = clean.words;
    } else {
      newWord += stringWithHtml[i];
      // Limpiar espacios que no se usan
      newWord = newWord.replace(/&nbsp;/i, '');
    }
    }
    // Devolvemos la lista con todas las palabras  y las palabras
    return {
      allWordPosition: this.allWordsElement,
      words: this.words
    };
  }

  toggleEmojiPicker(pickerStatus: boolean) {
    return !pickerStatus;
  }

  private locatePositionAndWordToInsert(selectWordItem: number, checkFocusPosition: number, insertChar: string) {
    const selectWord = this.words[selectWordItem];
    const checkIfWordFind = this.focusPosition - ( selectWord.length + checkFocusPosition);
    console.log('Diferencia para comprobar si está dentro', checkIfWordFind);
    if ( checkIfWordFind <= 0) {
        const changePosition = selectWord.length + checkIfWordFind;
        console.log('Hay que intercalar en este palabra en la posición ', changePosition, selectWord);
        console.log(selectWord.substring(0, changePosition) + insertChar + selectWord.substring(changePosition));
        this.words[selectWordItem] = selectWord.substring(0, changePosition) + insertChar + selectWord.substring(changePosition);
      } else {
        console.log('No encontrado', checkIfWordFind);
      }
    checkFocusPosition = selectWord.length + checkFocusPosition;
  }

  getCursorPosition() {
    /*if (document.getElementById('textEditor') === null) {
      document.getElementsByClassName('ck-content')[0].setAttribute('id', 'textEditor');
    }*/
    const range = window.getSelection().getRangeAt(0);
    const preCaretRange = range.cloneRange();
    preCaretRange.selectNodeContents(document.getElementById('editor'));
    preCaretRange.setEnd(range.endContainer, range.endOffset);
    const caretOffset = preCaretRange.toString().length;
    console.log('cratt', caretOffset);
    console.log('El cursor está en la posición', this.focusPosition);
  }
}
