export enum EMOJISSETS {
    NATIVE = 'native',
    GOOGLE = 'google',
    TWITTER = 'twitter',
    FACEBOOK = 'facebook',
    EMOJIONE = 'emojione',
    APPLE = 'apple',
    MESSENGER = 'messenger'
}