import { TimeAgoModule } from '../time-ago/time-ago.module';
import { ProfileImgModule } from '@core/@components/profile-img/profile-img.module';
import { PostComponent } from './post.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [PostComponent],
  imports: [
    CommonModule,
    TimeAgoModule,
    ProfileImgModule
  ],
  exports: [PostComponent]
})
export class PostModule { }
