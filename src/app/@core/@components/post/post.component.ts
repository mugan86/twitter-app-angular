import { Component, Input } from '@angular/core';
@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent {
  getPassTime: string;
  @Input() when: string = '2019-11-29T18:00:19.739+00:00';
  message = `This is a wider card with supporting text below as a natural lead-in
  to additional content. This content is a little bit longer.
  `;
  tags = '';

}
