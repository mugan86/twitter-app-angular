import { Injectable } from '@angular/core';
import * as moment from 'moment';
@Injectable({
  providedIn: 'root'
})
export class DataManageService {
  constructor() { 
    moment.locale('es');
  }

  getPassTimeFromNow(date: string) {
    return moment(date, "YYYY-MM-DD HH:mm:ss").fromNow();
  }
}
