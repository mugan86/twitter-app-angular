import { LikesComponent } from './../likes/likes.component';
import { FavsModule } from './../favs/favs.module';
import { TimelineComponent } from './timeline.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: TimelineComponent,
    children: [
      { path: 'home', loadChildren: () => import('./../home/home.module').then(m => m.HomeModule) },
      { path: 'notifications', loadChildren: () => import('./../notifications/notifications.module').then(m => m.NotificationsModule) },
      { path: 'messages', loadChildren: () => import('./../messages/messages.module').then(m => m.MessagesModule) },
      { path: 'follow/:type', loadChildren: () => import('./../follow/follow.module').then(m => m.FollowModule) },
      { path: 'likes', loadChildren: () => import('./../likes/likes.module').then(m => m.LikesModule) },
      { path: 'favs', loadChildren: () => import('./../favs/favs.module').then(m => m.FavsModule) },
      { path: `**`, redirectTo: `home`, pathMatch: `full` }
    ],
  },
  { path: `**`, redirectTo: `404`, pathMatch: `full` }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TimelineRoutingModule { }
