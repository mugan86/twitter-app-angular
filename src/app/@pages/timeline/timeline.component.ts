import { IVerticalMenuItem } from '../../@core/@components/vertical-menu/vertical-menu-item.interface';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.component.css']
})
export class TimelineComponent implements OnInit {
  verticalMenuItems: Array<IVerticalMenuItem> = []; 
  constructor() { }

  ngOnInit() {
    this.verticalMenuItems.push({icon: 'fas fa-home', title: 'Inicio', badge: false, route: '/home'});
    this.verticalMenuItems.push({icon: 'far fa-bell', title: 'Notificaciones', badge: true, route: '/notifications'});
    this.verticalMenuItems.push({icon: 'far fa-envelope', title: 'Mensajes', badge: false, route: '/messages'});
    this.verticalMenuItems.push({icon: 'fas fa-users', title: 'Seguidores', badge: false, route: '/follow/me'});
    this.verticalMenuItems.push({icon: 'fab fa-angellist', title: 'Siguiendo', badge: false, route: '/follow/to'});
    this.verticalMenuItems.push({icon: 'far fa-heart', title: 'Me gusta', badge: false, route: '/likes'});
    this.verticalMenuItems.push({icon: 'far fa-bookmark', title: 'Favoritos', badge: false, route: '/favs'});
    console.log(this.verticalMenuItems);
  }
}
