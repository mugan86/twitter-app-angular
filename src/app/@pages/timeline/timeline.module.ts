import { ProfileImgModule } from '@core/@components/profile-img/profile-img.module';
import { VerticalMenuModule } from '@core/@components/vertical-menu/vertical-menu.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TimelineRoutingModule } from './timeline-routing.module';
import { TimelineComponent } from './timeline.component';

@NgModule({
  declarations: [TimelineComponent],
  imports: [
    CommonModule,
    TimelineRoutingModule,
    VerticalMenuModule,
    ProfileImgModule
  ]
})
export class TimelineModule { }
