import { ProfileImgModule } from '@core/@components/profile-img/profile-img.module';
import { SearchInputModule } from '@core/@components/search-input/search-input.module';
import { PostModule } from '@core/@components/post/post.module';
import { EditorModule } from '@core/@components/editor/editor.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';


@NgModule({
  declarations: [HomeComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    PostModule,
    EditorModule,
    SearchInputModule,
    ProfileImgModule
  ]
})
export class HomeModule { }
