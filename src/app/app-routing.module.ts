import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./@pages/timeline/timeline.module').then(m => m.TimelineModule)
  },
  { path: '404', loadChildren: () => import('./@pages/notfound/notfound.module').then(m => m.NotfoundModule)}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
